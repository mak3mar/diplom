""" // // Скрипт для создания БД // // """

# Импортируем модуль для SQLite
import sqlite3

# Создаём подключение к базе данных. Если база данных не существует, она будет создана.
conn = sqlite3.connect('database.db')

# Создаём объект cursor, который используется для выполнения SQL команд.
cursor = conn.cursor()

# SQL скрипт, представленный как одна большая строка.
sql_script = """
-- Создание таблицы "Зона хранения"
CREATE TABLE IF NOT EXISTS StorageZone (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Name TEXT,
    Purpose TEXT,
    Location TEXT,
    CellCount INT
);

-- Создание таблицы "Ячейка"
CREATE TABLE IF NOT EXISTS Cell (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    NFC_Reader_Num TEXT,
    Max_Container_Weight REAL,
    Max_Container_Dimensions TEXT,
    Location TEXT,
    Status TEXT,
    StorageZone_ID INT,
    FOREIGN KEY (StorageZone_ID) REFERENCES StorageZone(ID)
);

-- Создание таблицы "Контейнер"
CREATE TABLE IF NOT EXISTS Container (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Type TEXT,
    Dimensions TEXT,
    Weight REAL,
    Status TEXT,
    Registration_Date TEXT,
    Description TEXT,
    Max_Object_Weight REAL
);

-- Создание таблицы "Объект"
CREATE TABLE IF NOT EXISTS Object (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Name TEXT,
    Category TEXT,
    Type TEXT,
    Dimensions TEXT,
    Weight REAL,
    Status TEXT,
    Registration_Date TEXT,
    Description TEXT
);

-- Создание таблицы "Пользователь"
CREATE TABLE IF NOT EXISTS User (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    User_Type TEXT,
    Last_Name TEXT,
    First_Name TEXT,
    Middle_Name TEXT,
    Department TEXT,
    Login TEXT,
    Password TEXT
);

-- Создание таблицы "Хранящийся контейнер"
CREATE TABLE IF NOT EXISTS StoredContainer (
    StorageZone_ID INT,
    Cell_ID INT,
    Container_ID INT,
    Operation_ID INT,
    User_ID INT,
    Storage_Start_Time TEXT,
    Object_Count INT,
    PRIMARY KEY (StorageZone_ID, Cell_ID, Container_ID, Operation_ID, User_ID),
    FOREIGN KEY (StorageZone_ID) REFERENCES StorageZone(ID),
    FOREIGN KEY (Cell_ID) REFERENCES Cell(ID),
    FOREIGN KEY (Container_ID) REFERENCES Container(ID),
    FOREIGN KEY (User_ID) REFERENCES User(ID)
);

-- Создание таблицы "Хранящийся объект"
CREATE TABLE IF NOT EXISTS StoredObject (
    StorageZone_ID INT,
    Cell_ID INT,
    Container_ID INT,
    Object_ID INT,
    Operation_ID INT,
    User_ID INT,
    Storage_Start_Time TEXT,
    PRIMARY KEY (StorageZone_ID, Cell_ID, Container_ID, Object_ID, Operation_ID, User_ID),
    FOREIGN KEY (StorageZone_ID) REFERENCES StorageZone(ID),
    FOREIGN KEY (Cell_ID) REFERENCES Cell(ID),
    FOREIGN KEY (Container_ID) REFERENCES Container(ID),
    FOREIGN KEY (Object_ID) REFERENCES Object(ID),
    FOREIGN KEY (User_ID) REFERENCES User(ID)
);

-- Создание таблицы "Пустой контейнер"
CREATE TABLE IF NOT EXISTS EmptyContainer (
    Container_ID INT,
    User_ID INT,
    Operation_ID INT,
    Idle_Start_Time TEXT,
    PRIMARY KEY (Container_ID, User_ID, Operation_ID),
    FOREIGN KEY (Container_ID) REFERENCES Container(ID),
    FOREIGN KEY (User_ID) REFERENCES User(ID)
);

-- Создание таблицы "Изъятый объект"
CREATE TABLE IF NOT EXISTS WithdrawnObject (
    Object_ID INT,
    User_ID INT,
    Operation_ID INT,
    Withdrawal_Time TEXT,
    Withdrawal_Reason TEXT,
    Withdrawal_Period TEXT,
    PRIMARY KEY (Object_ID, User_ID, Operation_ID),
    FOREIGN KEY (Object_ID) REFERENCES Object(ID),
    FOREIGN KEY (User_ID) REFERENCES User(ID)
);

-- Создание таблицы "Списанный объект"
CREATE TABLE IF NOT EXISTS WrittenOffObject (
    Object_ID INT,
    User_ID INT,
    Operation_ID INT,
    WriteOff_Time TEXT,
    WriteOff_Reason TEXT,
    PRIMARY KEY (Object_ID, User_ID, Operation_ID),
    FOREIGN KEY (Object_ID) REFERENCES Object(ID),
    FOREIGN KEY (User_ID) REFERENCES User(ID)
);

-- Создание таблицы "Документация"
CREATE TABLE IF NOT EXISTS Documentation (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Document_Name TEXT,
    Document_Type TEXT,
    Current_Location TEXT,
    Object_ID INT,
    FOREIGN KEY (Object_ID) REFERENCES Object(ID)
);

-- Создание таблицы "История операций"
CREATE TABLE IF NOT EXISTS OperationHistory (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Operation_Type TEXT,
    Time TEXT,
    Status TEXT,
    User_ID INT,
    StorageZone_ID INT,
    Cell_ID INT,
    Container_ID INT,
    Object_ID INT,
    FOREIGN KEY (User_ID) REFERENCES User(ID),
    FOREIGN KEY (StorageZone_ID) REFERENCES StorageZone (ID) ON DELETE SET NULL ON UPDATE SET NULL,
    FOREIGN KEY (Cell_ID) REFERENCES Cell (ID) ON DELETE SET NULL ON UPDATE SET NULL,
    FOREIGN KEY (Container_ID) REFERENCES Container (ID) ON DELETE SET NULL ON UPDATE SET NULL,
    FOREIGN KEY (Object_ID) REFERENCES Object (ID) ON DELETE SET NULL ON UPDATE SET NULL
);
"""

# Исполняем SQL команды.
cursor.executescript(sql_script)

# Закрываем подключение к базе данных.
conn.close()

""" // // Скрипт для создания БД (конец) // // """