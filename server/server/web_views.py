# Импорт ModelView из flask_admin и SQLAlchemy сессии из нашего приложения
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from server import app, db

# Импортируем все классы для работы с таблицами (не можем использовать * во избежание конфликта с db)
from server.db.models import StorageZone, Cluster, Cell, Container, Object, User, OperationHistory, StoredContainer, StoredObject

"""Определение представления администратора, наследуя ModelView"""
class StorageZoneAdmin(ModelView):
    column_searchable_list = ('ID','Name','Purpose','Location',)
    column_labels = {'Name': 'Название',
                     'Purpose': 'Назначение',
                     'Location': 'Месторасположение',
                     'ClusterCount': 'Количество кластеров в зоне'}
    pass

class ClusterAdmin(ModelView):
    column_list = ('ID','StorageZone_ID','Module_IP', 'Name', 'Location', 'Status',)
    form_columns = ('ID','StorageZone_ID','Module_IP', 'Name', 'Location',)
    column_searchable_list = ('ID','Module_IP','Name','Location',)
    column_labels = {'Module_IP': 'IP-адрес модуля связи',
                     'StorageZone_ID': 'ID зоны хранения',
                     'Name': 'Название',
                     'Location': 'Месторасположение',
                     'CellCount': 'Количество ячеек в кластере',
                     'Status': 'Статус'}
    pass

class CellAdmin(ModelView):
    column_list = ('ID','Cluster_ID','Reader_ID', 'Location', 'Status',)
    form_columns = ('ID','Cluster_ID','Reader_ID', 'Location', 'Status',)
    column_searchable_list = ('ID','Reader_ID','Location','Status',)
    column_labels = {'Reader_ID': 'Номер считывателя',
                     'Cluster_ID': 'ID кластера',
                     'Max_Weight': 'Максимальный вес',
                     'Max_Dimensions': 'Максимальный размер',
                     'Location': 'Месторасположение',
                     'Status': 'Статус ячейки'}
    pass

class ContainerAdmin(ModelView):
    form_columns = ('ID','Type','Dimensions','Weight','Max_Weight','Description','Status',)
    column_searchable_list = ('ID','Type','Description','Status',)
    column_labels = {'Type': 'Тип',
                     'Dimensions': 'Размер',
                     'Weight': 'Вес',
                     'Max_Weight': 'Максимальный вес',
                     'Description': 'Описание',
                     'Status': 'Статус контейнера'}
    pass

class ObjectAdmin(ModelView):
    column_list = ('ID','Name','Category','Type','Dimensions','Weight','Description','Status',)
    form_columns = ('ID','Name','Category','Type','Dimensions','Weight','Description','Status',)
    column_searchable_list = ('ID','Name','Type','Category','Description','Status',) 
    column_labels = {'Name': 'Название',
                     'Category': 'Категория',
                     'Type': 'Тип',
                     'Dimensions': 'Размер',
                     'Weight': 'Вес',
                     'Description': 'Описание',
                     'Status': 'Статус объекта'}
    pass

class UserAdmin(ModelView):
    column_exclude_list = ('login', 'password',)
#    form_columns = ('ID','Name','Type','Category','Description','Status',)
    column_searchable_list = ('ID','Type','Last_Name','First_Name','Middle_Name','Department',)
    column_labels = {'Type': 'Тип пользователя',
                     'Last_Name': 'Фамилия',
                     'First_Name': 'Имя',
                     'Middle_Name': 'Отчество',
                     'Department': 'Отдел/Должность'}
    pass

class OperationHistoryAdmin(ModelView):
#    form_columns = ('ID','Type','Time','Status',)
    column_list = ('ID', 'Cell_ID', 'Container_ID','Object_ID','Type','Time','Status',)
    column_searchable_list = ('ID','Type','Time','Status',)
    column_labels = {'ID': 'ID операции',
                     'StorageZone_ID': 'ID зоны',
                     'Cluster_ID': 'ID кластера',
                     'Cell_ID': 'ID ячейки',
                     'Container_ID': 'ID контейнера',
                     'Object_ID': 'ID объекта',
                     'Type': 'Тип операции',
                     'Time': 'Время операции',
                     'Status': 'Статус операции'}
    pass

class StoredContainerAdmin(ModelView):    
    column_list = ('Container_ID','container.Type', 'Cell_ID', 'cell.Location', 'Start_Time','Object_Count','Operation_ID',)
    form_columns = ('Container_ID', 'Cell_ID', 'Start_Time','Object_Count','Operation_ID',)
    column_searchable_list = ('Container_ID','Start_Time','Object_Count')
    column_labels = {'Container_ID': 'ID контейнера',
                     'container.Type': 'Тип контейнера', 
                     'Cell_ID': 'ID ячейки', 
                     'cell.Location': 'Месторасположение ячейки',
                     'Start_Time': 'Время начала хранения',
                     'Object_Count': 'Количество хранимых объектов',
                     'Operation_ID': 'ID операции'}
    pass

class StoredObjectAdmin(ModelView):
    column_list = ('Object_ID','object.Name', 'Container_ID', 'Cell_ID', 'cell.Location', 'Start_Time','Operation_ID',)
    form_columns = ('Object_ID', 'Cell_ID', 'Container_ID', 'Start_Time','Operation_ID',)
    column_searchable_list = ('Object_ID','Start_Time',)
    column_labels = {'Object_ID': 'ID объекта',
                     'object.Name': 'Название объекта',
                     'Container_ID': 'Номер контейнера', 
                     'Cell_ID': 'ID ячейки', 
                     'cell.Location': 'Месторасположение ячейки',
                     'Start_Time': 'Время начала хранения',
                     'Operation_ID': 'ID операции'}
    pass

""" Создание экземпляров представлений """
storage_zone_admin_view = StorageZoneAdmin(StorageZone, db.session, name = 'Зона хранения')
cluster_admin_view = ClusterAdmin(Cluster, db.session, name = 'Кластер ячеек')
cell_admin_view = CellAdmin(Cell, db.session, name = 'Ячейка')
container_admin_view = ContainerAdmin(Container, db.session, name = 'Контейнер')
object_admin_view = ObjectAdmin(Object, db.session, name = 'Объект')
user_admin_view = UserAdmin(User, db.session, name = 'Пользователь')
operation_history_admin_view = OperationHistoryAdmin(OperationHistory, db.session, name = 'История операций')
stored_container_admin_view = StoredContainerAdmin(StoredContainer, db.session, name = 'Хранящийся контейнер')
stored_object_admin_view = StoredObjectAdmin(StoredObject, db.session, name = 'Хранящийся объект')


""" Создание экземпляра Admin и добавление представлений """
# Создаем экземпляр Admin без связывания с приложением
# Инициализация будет проведена в __init__.py
admin = Admin(name='Веб-форма доступа для Системы Автоматизированного Позиционирования Контейнеров-Хранилищ', 
              template_mode='bootstrap3', 
              index_view=AdminIndexView(url=app.config['ADMIN_INDEX_URL']))

admin._menu = admin._menu[1:] # Удаляем кнопку "Home" (!!!Потом можно чем-нибудь заменить)

# Добавляем представление к админу
admin.add_view(storage_zone_admin_view)
admin.add_view(cluster_admin_view)
admin.add_view(cell_admin_view)
admin.add_view(container_admin_view)
admin.add_view(object_admin_view)
admin.add_view(user_admin_view)
admin.add_view(operation_history_admin_view)
admin.add_view(stored_container_admin_view)
admin.add_view(stored_object_admin_view)
