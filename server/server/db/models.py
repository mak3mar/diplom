""" // // Файл для создания моделей таблиц в БД // // """
# Примечание: модель БД создаётся именно здесь, но файл с БД создаётся в run.py
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy() # Инициализируем экземпляр SQLAlchemy.

# Сущности: Зона хранения, Кластер ячеек, Ячейка, Контейнер, Объект, Пользователь, История операций.

## Таблицы основных сущностей
# Создание модели "Зона хранения"
# Зона хранения может содержать множество кластеров ячеек
class StorageZone(db.Model):
    __tablename__ = 'StorageZone'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждой зоны

    # Устанавливаем взаимосвязи:
    clusters = db.relationship('Cluster', back_populates='storageZone', lazy=True) # с Кластерами, которые входят в зону

    operations = db.relationship('OperationHistory', back_populates='storageZone', lazy=True) # с Операциями, которые проводились в зоне

    # Дополнительные атрибуты
    Name = db.Column(db.Text, nullable=True) # Название зоны
    Purpose = db.Column(db.Text, nullable=True) # Назначение зоны
    Location = db.Column(db.Text, nullable=True) # Месторасположение зоны
    ClusterCount = db.Column(db.Integer, nullable=True) # Количество кластеров в зоне
    """
    взаимодействие с пользователем (добавление, изменение, удаление)
    """

# Создание модели "Кластер ячеек"
# Кластер ячеек может содержать множество ячеек
# Один кластер ячеек принадлежит строго к одной зоне хранения
class Cluster(db.Model):
    __tablename__ = 'Cluster'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждого кластера

    # Устанавливаем взаимосвязи:
    StorageZone_ID = db.Column(db.Integer, db.ForeignKey('StorageZone.ID'), nullable=False) # с Зоной Хранения, в которой расположен кластер
    storageZone = db.relationship('StorageZone', back_populates='clusters', lazy=True)

    cells = db.relationship('Cell', back_populates='cluster', lazy=True) # с Ячейками, которые входят в кластер

    operations = db.relationship('OperationHistory', back_populates='cluster', lazy=True) # с Операциями, которые проводились с кластером

    # Дополнительные атрибуты
    Module_IP = db.Column(db.Text, nullable=False) # IP-адрес модуля связи
    Name = db.Column(db.Text, nullable=True) # Название кластера
    Location = db.Column(db.Text, nullable=True) # Месторасположение кластера
    CellCount = db.Column(db.Integer, nullable=True) # Количество ячеек в кластере
    Status = db.Column(db.Text, nullable=True) # Статус кластера (Онлайн/Не в сети)
    """
    взаимодействие при (пере)инициализации модуля связи (добавление, изменение, удаление)
    взаимодействие с пользователем (добавление, изменение, удаление)
    """

# Создание модели "Ячейка"
# Ячейка может хранить строго один контейнер
# Одна ячейка принадлежит строго к одному кластеру ячеек
class Cell(db.Model):
    __tablename__ = 'Cell'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждой ячейки
    
    # Устанавливаем взаимосвязи:
    Cluster_ID = db.Column(db.Integer, db.ForeignKey('Cluster.ID'), nullable=False) # с Кластером, к которому относится ячейка
    cluster = db.relationship('Cluster', back_populates='cells', lazy=True)

    container = db.relationship('StoredContainer', uselist=False, back_populates='cell', lazy=True) # с Хранимым Контейнером
    
    object = db.relationship('StoredObject', back_populates='cell', lazy=True) # с Хранимым Объектом

    operations = db.relationship('OperationHistory', back_populates='cell', lazy=True) # с Операциями, которые проводились с кластером

    # Дополнительные атрибуты
    Reader_ID = db.Column(db.Text, nullable=False) # Номер считывателя (в составе кластера)
    Max_Weight = db.Column(db.Float, nullable=True) # Максимальный вес, который может выдержать ячейка
    Max_Dimensions = db.Column(db.Text, nullable=True) # Максимальный размер, который может вместить ячейка
    Location = db.Column(db.Text, nullable=True) # Месторасположение ячейки
    Status = db.Column(db.Text, nullable=False) # Статус ячейки (Занята/свободна)
    """
    взаимодействие при (пере)инициализации модуля связи (добавление, изменение, удаление)
    взаимодействие с пользователем (добавление, изменение, удаление)
    """

# Создание модели "Контейнер"
# Контейнер может хранить множество объектов
# Один контейнер может лежать только в одной 
class Container(db.Model):
    __tablename__ = 'Container'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждого контейнера

    # Устанавливаем взаимосвязи:
    storedContainer = db.relationship('StoredContainer', back_populates='container', uselist=False) # С Хранящимся контейнером

    operations = db.relationship('OperationHistory', back_populates='container', lazy=True) # с Операциями, которые проводились с контейнером

    # Дополнительные атрибуты
    Type = db.Column(db.Text, nullable=True) # Тип контейнера
    Dimensions = db.Column(db.Text, nullable=True) # Размер контейнера
    Weight = db.Column(db.Float, nullable=True) # Вес контейнера
    Max_Weight = db.Column(db.Float, nullable=True) # Максимальный вес, который может выдержать контейнер
    Description = db.Column(db.Text, nullable=True) # Словесное описание контейнера
    Status = db.Column(db.Text, nullable=False) # Статус контейнера (На хранении/изъят) (!!! Расширение: Занят/Свободен)
    """
    взаимодействие с пользователем (добавление, удаление, изменение)
    взаимодействие при прикладывании метки (добавление, если раньше отсутствовал в системе)
    """

# Создание модели "Объект"
# Объект может иметь множество дополнений к себе
# Один объект может лежать только в одном контейнере
class Object(db.Model):
    __tablename__ = 'Object'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждого объекта

    # Устанавливаем взаимосвязи:
    storedObject = db.relationship('StoredObject', back_populates='object', uselist=False) # С Хранящимся объектом

    operations = db.relationship('OperationHistory', back_populates='object', lazy=True) # с Операциями, которые проводились с объектом

    # Дополнительные атрибуты
    Name = db.Column(db.Text, nullable=True) # Название объекта
    Category = db.Column(db.Text, nullable=True) # Категория объекта
    Type = db.Column(db.Text, nullable=True) # Тип объекта
    Dimensions = db.Column(db.Text, nullable=True) # Размер объекта
    Weight = db.Column(db.Float, nullable=True) # Вес объекта
    Description = db.Column(db.Text, nullable=True) # Словесное описание объекта
    Status = db.Column(db.Text, nullable=False) # Статус объекта (Хранится/Изъят/Списан)
    """
    взаимодействие с пользователем (добавление, удаление, изменение)
    взаимодействие при прикладывании метки (добавление, если раньше отсутствовал в системе)
    """

# Создание модели "Пользователь"
# Пользователь может выполнять множество операций
class User(db.Model):
    __tablename__ = 'User'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждого пользователя

    # Устанавливаем взаимосвязи:
    operations = db.relationship('OperationHistory', back_populates='user', lazy=True) # с Операциями, которые проводил пользователь

    # Дополнительные атрибуты
    Type = db.Column(db.Text, nullable=True) # Тип пользователя (Пользователь/Админ)
    Last_Name = db.Column(db.Text, nullable=True) # Фамилия
    First_Name = db.Column(db.Text, nullable=True) # Имя
    Middle_Name = db.Column(db.Text, nullable=True) # Отчество (или второе имя)
    Department = db.Column(db.Text, nullable=True) # Отдел (или иная категория различения пользователей)
    Login = db.Column(db.Text, nullable=False) # Логин пользователя
    Password = db.Column(db.Text, nullable=False) # Пароль пользователя
    """
    взаимодействие с пользователем (добавление, изменение, удаление)
    """

# Создание модели "История операций"
# Одна операция может быть соврешена строго одним пользователем
class OperationHistory(db.Model):
    __tablename__ = 'OperationHistory'
    ID = db.Column(db.Integer, primary_key=True) # Уникальность каждой операции

    # Устанавливаем взаимосвязи:
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), nullable=True) # с Пользователем (не всегда присутствует в операциях)
    user = db.relationship('User', back_populates='operations', lazy=True)

    StorageZone_ID = db.Column(db.Integer, db.ForeignKey('StorageZone.ID'), nullable=True) # с Зоной, в которой произошла операция
    storageZone = db.relationship('StorageZone', back_populates='operations', lazy=True)

    Cluster_ID = db.Column(db.Integer, db.ForeignKey('Cluster.ID'), nullable=True) # с Кластером, в котором произошла операция
    cluster = db.relationship('Cluster', back_populates='operations', lazy=True)

    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), nullable=True) # с Ячейкой, с которой произошла операция
    cell = db.relationship('Cell', back_populates='operations', lazy=True)

    Container_ID = db.Column(db.Integer, db.ForeignKey('Container.ID'), nullable=True) # с Контейнером, с которым произошла операция
    container = db.relationship('Container', back_populates='operations', lazy=True)

    Object_ID = db.Column(db.Integer, db.ForeignKey('Object.ID'), nullable=True) # с Объектом, с которым произошла операция
    object = db.relationship('Object', back_populates='operations', lazy=True)

    storedContainer = db.relationship('StoredContainer', back_populates='operation', uselist=False, lazy=True) # с Хранимым Контейнером

    storedObject = db.relationship('StoredObject', back_populates='operation', uselist=False, lazy=True) # с Хранимым Объектом

    # Дополнительные атрибуты
    Type = db.Column(db.Text, nullable=True) # Тип операции
    Time = db.Column(db.DateTime, nullable=True) # Время операции
    Status = db.Column(db.Text, nullable=False) # Статус операции (Успешно/Ошибка)
    """
    Автоматическое взаимодействие в любом случае (всегда)
    """

## Таблицы связей
# Создание модели "Хранящийся контейнер"
# Отражает связь 1:1 (Контейнер : Ячейка)
class StoredContainer(db.Model):
    __tablename__ = 'StoredContainer'
    Container_ID = db.Column(db.Integer, db.ForeignKey('Container.ID'), primary_key=True) # Уникальный идентификатор контейнера
    container = db.relationship('Container', back_populates='storedContainer')

    # Устанавливаем взаимосвязи:
    storedObjects = db.relationship('StoredObject', back_populates='storedContainer', lazy=True) # С Хранимым Объектом

    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), nullable=False)  # с Хранящей ячейкой
    cell = db.relationship('Cell', back_populates='container', lazy=True)

    Operation_ID = db.Column(db.Integer, db.ForeignKey('OperationHistory.ID'), nullable=False) # С Операцией
    operation = db.relationship('OperationHistory', back_populates='storedContainer', lazy=True)

    # Дополнительные атрибуты
    Start_Time = db.Column(db.DateTime, nullable=True) # Время начала хранения
    Object_Count = db.Column(db.Integer, nullable=True) # Количество хранимых объектов
    """
    взаимодействие при прикладывании метки (добавление в любом случае)
    взаимодействие при убирании метки (удаление в любом случае)
    """

# Создание модели "Хранящийся объект"
# Отражает связь 1:М (Контейнер : Объект)
class StoredObject(db.Model):
    __tablename__ = 'StoredObject'
    Object_ID = db.Column(db.Integer, db.ForeignKey('Object.ID'), primary_key=True) # Уникальный идентификатор объекта
    object = db.relationship('Object', back_populates='storedObject') # С Хранящимся объектом

    # Устанавливаем взаимосвязи:
    Container_ID = db.Column(db.Integer, db.ForeignKey('StoredContainer.Container_ID'), nullable=False) # С Хранящим Контейнером
    storedContainer = db.relationship('StoredContainer', back_populates='storedObjects', lazy=True) 

    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), nullable=False)  # С Хранящей ячейкой
    cell = db.relationship('Cell', back_populates='object', lazy=True)

    Operation_ID = db.Column(db.Integer, db.ForeignKey('OperationHistory.ID'), nullable=False) # С Операцией
    operation = db.relationship('OperationHistory', back_populates='storedObject', lazy=True)

    # Дополнительные атрибуты
    Start_Time = db.Column(db.DateTime, nullable=True) # Время начала хранения
    """
    взаимодействие при прикладывании метки (добавление при условии наличия данных об объекте)
    взаимодействие при убирании метки (удаление при условии наличия данных об объекте)
    """

""" // // Файл для создания моделей таблиц в БД (конец) // // """
