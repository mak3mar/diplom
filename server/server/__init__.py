""" // // Файл инициализации сервера // // """

from flask import Flask # Flask - это класс, который представляет веб-приложение
#from flask_admin import Admin # 
#from flask_admin.contrib.sqla import ModelView

app = Flask(__name__) # Создаем экземпляр класса Flask. Это наше веб-приложение
app.config.from_object('config') # Загрузка конфигурационных переменных из файла config.py

from server.db.models import * # Подключаем все классы моделей БД из файла models.py
db.init_app(app) # Инициализируем БД с нашем приложением (первичная инициализация во views.py)

from server.web_views import admin # Импорт представления администратора из файла admin_views.py
admin.init_app(app) # Создание экземпляра Flask-Admin с нашим приложением Flask в качестве параметра

from server.views import * # Импортируем маршруты после создания объекта app (нельзя использовать import views из-за работы декораторов)

""" // // Файл инициализации сервера (конец) // // """