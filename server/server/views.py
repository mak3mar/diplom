""" // // Файл для обработки маршрутов // // """
"""" Подключение модулей """
# request - это объект, который содержит информацию о текущем веб-запросе, который обрабатывает Flask
from flask import request, jsonify
from datetime import datetime
from sqlalchemy.exc import SQLAlchemyError

from server import app, db  # Импортируем объект Flask app
# Импортируем все классы для работы с таблицами (не можем использовать * во избежание конфликта с db)
from server.db.models import StorageZone, Cluster, Cell, Container, Object, User, OperationHistory, StoredContainer, StoredObject


""" Обработка запросов """
# Декоратор app.route создает маршрут URL для веб-приложения.
@app.route('/update', methods=['POST'])
def update_data():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON request"}), 400

    data = request.get_json()

    # Вывод данных в консоль
    print(data)
    
    if 'cluster' in data:
        cluster_data = data['cluster']
        if 'ID' not in cluster_data:
            return jsonify({"msg": "Missing 'ID' in cluster data"}), 400
        
        cluster = Cluster.query.filter_by(ID=cluster_data['ID']).first()
        if cluster:
            # Обновление данных, если кластер уже существует
            for key, value in cluster_data.items():
                setattr(cluster, key, value)
        else:
            # Добавление нового кластера, если он не существует
            cluster = Cluster(**cluster_data)
            db.session.add(cluster)

    if 'cell' in data:
        cell_data = data['cell']
        if 'ID' not in cell_data:
            return jsonify({"msg": "Missing 'ID' in cell data"}), 400
        
        cell = Cell.query.filter_by(ID=cell_data['ID']).first()
        if cell:
            for key, value in cell_data.items():
                setattr(cell, key, value)
        else:
            cell = Cell(**cell_data)
            db.session.add(cell)

    if 'container' in data:
        container_data = data['container']
        if 'ID' not in container_data:
            return jsonify({"msg": "Missing 'ID' in container data"}), 400

        # Извлекаем данные, которые должны пойти в таблицу Container
        basic_container_data = {key: container_data[key] for key in ['ID', 'Type', 'Status'] if key in container_data}

        # Создаем или обновляем запись в таблице Container
        container = Container.query.filter_by(ID=basic_container_data['ID']).first()
        if container:
            for key, value in basic_container_data.items():
                setattr(container, key, value)
        else:
            container = Container(**basic_container_data)
            db.session.add(container)

        # Извлекаем данные, которые должны пойти в таблицу StoredContainer
        stored_container_data = {key: container_data[key] for key in ['ID', 'Cell_ID'] if key in container_data}

        stored_container = StoredContainer.query.filter_by(Container_ID=stored_container_data['ID']).first()
        if stored_container:
            # Проверяем статус контейнера
            if basic_container_data.get('Status') == 'Изъят':
                # Удаляем хранимый контейнер, если статус контейнера 'Изъят'
                db.session.delete(stored_container)

                # Создаем запись в истории операций
                operation = OperationHistory(Type='Delete', Time=datetime.now(), Status='Success',
                                            Container_ID=stored_container.Container_ID, Cell_ID=stored_container.Cell_ID)
                db.session.add(operation)
                db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена

            else:
                for key, value in stored_container_data.items():
                    if hasattr(stored_container, key):
                        setattr(stored_container, key, value)

                # Обновляем время начала хранения
                stored_container.Start_Time = datetime.now()

                # Создаем запись в истории операций
                operation = OperationHistory(Type='Update', Time=datetime.now(), Status='Success',
                                            Container_ID=stored_container.Container_ID, Cell_ID=stored_container.Cell_ID)
                db.session.add(operation)
                db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена

                # Обновляем Operation_ID для StoredContainer
                stored_container.Operation_ID = operation.ID

        else:
            # Изменяем ключ 'ID' на 'Container_ID', чтобы соответствовать модели StoredContainer
            stored_container_data['Container_ID'] = stored_container_data.pop('ID')

            # Создаем запись в истории операций
            operation = OperationHistory(Type='Create', Time=datetime.now(), Status='Success',
                                        Container_ID=stored_container_data['Container_ID'], Cell_ID=stored_container_data['Cell_ID'])
            db.session.add(operation)
            db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена

            # Добавляем Operation_ID в данные контейнера и создаем хранимый контейнер
            stored_container_data['Operation_ID'] = operation.ID
            stored_container = StoredContainer(**stored_container_data, Start_Time=datetime.now())
            db.session.add(stored_container)

    if 'object' in data:
        object_data = data['object']
        if 'ID' not in object_data:
            return jsonify({"msg": "Missing 'ID' in object data"}), 400

        # Извлекаем данные, которые должны пойти в таблицу Object
        basic_object_data = {key: object_data[key] for key in ['ID', 'Name', 'Category', 'Type', 'Status'] if key in object_data}

        # Создаем или обновляем запись в таблице Object
        object = Object.query.filter_by(ID=basic_object_data['ID']).first()
        if object:
            for key, value in basic_object_data.items():
                setattr(object, key, value)
        else:
            object = Object(**basic_object_data)
            db.session.add(object)

        # Извлекаем данные, которые должны пойти в таблицу StoredObject
        stored_object_data = {key: object_data[key] for key in ['ID', 'Container_ID', 'Cell_ID'] if key in object_data}

        stored_object = StoredObject.query.filter_by(Object_ID=stored_object_data['ID']).first()
        if stored_object:
            # Проверяем статус объекта
            if basic_object_data.get('Status') == 'Изъят':
                # Удаляем хранимый объект, если статус объекта 'Изъят'
                db.session.delete(stored_object)

                # Создаем запись в истории операций
                operation = OperationHistory(Type='Delete', Time=datetime.now(), Status='Success',
                                            Object_ID=stored_object.Object_ID, Cell_ID=stored_object.Cell_ID,
                                            Container_ID=stored_object.Container_ID)
                db.session.add(operation)
                db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена
            else:
                for key, value in stored_object_data.items():
                    if hasattr(stored_object, key):
                        setattr(stored_object, key, value)

                # Обновляем время начала хранения
                stored_object.Start_Time = datetime.now()

                # Создаем запись в истории операций
                operation = OperationHistory(Type='Update', Time=datetime.now(), Status='Success',
                                            Object_ID=stored_object.Object_ID, Cell_ID=stored_object.Cell_ID,
                                            Container_ID=stored_object.Container_ID)
                db.session.add(operation)
                db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена

                # Обновляем Operation_ID для StoredObject
                stored_object.Operation_ID = operation.ID

        else:
            # Изменяем ключ 'ID' на 'Object_ID', чтобы соответствовать модели StoredObject
            stored_object_data['Object_ID'] = stored_object_data.pop('ID')

            # Создаем запись в истории операций
            operation = OperationHistory(Type='Create', Time=datetime.now(), Status='Success',
                                        Object_ID=stored_object_data['Object_ID'], Cell_ID=stored_object_data['Cell_ID'],
                                        Container_ID=stored_object_data['Container_ID'])
            db.session.add(operation)
            db.session.flush()  # Используем flush(), чтобы получить ID операции до того, как она будет закоммичена

            # Добавляем Operation_ID в данные объекта и создаем хранимый объект
            stored_object_data['Operation_ID'] = operation.ID
            stored_object = StoredObject(**stored_object_data, Start_Time=datetime.now())
            db.session.add(stored_object)
    
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback() # Откатываем изменения в случае ошибки
        app.logger.error(f"Failed to commit changes to database: {str(e)}")
        return jsonify({"msg": "Failed to commit changes to database"}), 500

    return jsonify({"msg": "Data updated successfully"}), 200


""" // // Файл для обработки маршрутов (конец) // // """