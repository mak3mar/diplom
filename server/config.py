""" // // Конфигурационный файл // // """

""" Подключение библиотек и модулей"""
import os # Импорт модуля для работы с операционной системой

""" Объявление констант """
DATABASE_PATH = "/server/db" # Путь к БД
DATABASE_NAME = '/database.db' # Имя файла БД

# Указываем путь до базы данных, здесь мы используем SQLite
SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.abspath(os.getcwd())+DATABASE_PATH+DATABASE_NAME
#SQLALCHEMY_TRACK_MODIFICATIONS = False # Отключает для SQLAlchemy отслеживание изменений в объектах и выдачу сигналов об этом

# Секретный ключ для Flask-Admin
ADMIN_INDEX_URL = '/SAPCH'

# Секретный ключ для Flask-Admin
SECRET_KEY = 'your-secret-key'

""" // // Конфигурационный файл (конец) // // """